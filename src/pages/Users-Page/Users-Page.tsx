import { Box } from "@material-ui/core";
import * as React from "react";
import UsersTable from "../../components/Users-Table/Users-Table";

export interface UsersPageProps {}

const UsersPage: React.SFC<UsersPageProps> = () => {
  return (
    <Box>
      <UsersTable />
    </Box>
  );
};

export default UsersPage;
