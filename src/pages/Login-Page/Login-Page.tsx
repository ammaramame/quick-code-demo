import { Box, Grid } from "@material-ui/core";
import { Formik } from "formik";
import * as React from "react";
import LoginCard from "../../components/Login-Card/Login-Card";
import { UseLoginPageProps } from "./Login-Page-Hook";

export interface LoginPageProps {}

const LoginPage: React.FC<LoginPageProps> = () => {
  const [LoginErrorsArray, SetLoginErrorsArray] = React.useState<string[]>([]);
  const { FormikHookProps, LoginCardHookProps } = UseLoginPageProps();
  return (
    <Box>
      <Formik {...FormikHookProps(SetLoginErrorsArray)}>
        {(FormikProp) => (
          <form onSubmit={FormikProp.handleSubmit}>
            <Grid container justifyContent="center">
              <Grid item lg={6} md={8} xs={12}>
                <LoginCard
                  {...LoginCardHookProps(FormikProp, LoginErrorsArray)}
                />
              </Grid>
            </Grid>
          </form>
        )}
      </Formik>
    </Box>
  );
};

export default LoginPage;
