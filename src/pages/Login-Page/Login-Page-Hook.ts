import { FormikConfig, FormikProps } from "formik";
import React from "react";
import * as yup from "yup";
import { GetUsers } from "../../api/User/Get-User";
import { LoginCardProps } from "../../components/Login-Card/Login-Card";
import { Login } from "../../utils/User/Login";

interface Fields {
  username: string;
  password: string;
  remember: boolean;
}

const ValidationSchema = yup.object().shape({
  username: yup.string().required(),
  password: yup.string().required(),
});

const LoginCardHookProps = (
  FormikProp: FormikProps<Fields>,
  LoginErrorsArray: string[]
) =>
  ({
    onPasswordChange: (e) =>
      FormikProp.setFieldValue("password", e.currentTarget.value),
    onUsernameChange: (e) =>
      FormikProp.setFieldValue("username", e.currentTarget.value),
    OnButtonClick: () => FormikProp.submitForm(),
    PasswordValue: FormikProp.values["password"],
    UsernameValue: FormikProp.values["username"],
    RememberMeValue: FormikProp.values["remember"],
    UsernamevalueErrorHelperText: FormikProp.errors["username"],
    PasswordvalueErrorHelperText: FormikProp.errors["password"],
    DisableConfirm: FormikProp.isSubmitting,
    errorsArray: LoginErrorsArray,
    onRememberMeChange: (checked) =>
      FormikProp.setFieldValue("remember", checked),
  } as LoginCardProps);

const FormikHookProps = (
  SetLoginErrorsArray: React.Dispatch<React.SetStateAction<string[]>>
) =>
  ({
    validateOnBlur: false,
    validateOnChange: false,
    validationSchema: ValidationSchema,
    initialValues: { password: "", username: "", remember: true },
    onSubmit: (values, action) => {
      action.setSubmitting(true);
      GetUsers(
        {
          match: {
            username: values["username"],
            password: values["password"],
          },
        },
        (error, resp) => {
          action.setSubmitting(false);
          if (error) {
            console.log(error.message);
          } else if (resp) {
            if (resp.items.length > 0) {
              Login(resp.items[0], values["remember"]);
            } else {
              SetLoginErrorsArray(["Username or password is not exist!"]);
            }
          }
        }
      );
    },
  } as FormikConfig<Fields>);

export const UseLoginPageProps = () => {
  return { FormikHookProps, LoginCardHookProps };
};
