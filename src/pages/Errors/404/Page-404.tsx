import { Box, Typography } from "@material-ui/core";
import * as React from "react";

export interface Page404Props {}

const Page404: React.SFC<Page404Props> = () => {
  return (
    <Box textAlign="center">
      <Typography variant="h1">404</Typography>
      <Typography variant="h3">PAGE NOT FOUND :(</Typography>
    </Box>
  );
};

export default Page404;
