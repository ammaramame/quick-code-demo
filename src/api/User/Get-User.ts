import { AxiosError } from "axios";
import { axiosInstance } from "../../config/axios";
import { User } from "../../models/User";
import MockAdapter from "axios-mock-adapter";
import { UsersArray } from "../../Mocks/Users";

let mock = new MockAdapter(axiosInstance, { delayResponse: 1500 });
mock.onGet("/users").reply((config) => [
  200,
  {
    items: config.params.match
      ? UsersArray.filter(
          (item) =>
            item.username === config.params?.match?.username &&
            item.password === config?.params?.match.password
        )
      : config.params?.searchUser
      ? UsersArray.filter(
          (item) => item.username.indexOf(config.params.searchUser) > -1
        )
      : UsersArray,
  },
]);

interface Params {
  searchUser?: string;
  match?: {
    username: string;
    password: string;
  };
}
interface Resp {
  items: User[];
}

type GetUsersType = (
  Params: Params,
  callback: (error: null | AxiosError, Resp: null | Resp) => any
) => any;
const GetUsers: GetUsersType = (Params, callback) => {
  axiosInstance
    .get("/users", {
      params: {
        searchUser: Params.searchUser?.toLowerCase(),
        match: Params.match,
      },
    })
    .then((resp) => callback(null, resp.data))
    .catch((error) => callback(error, null));
};

export { GetUsers };
