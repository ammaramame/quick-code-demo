import Cookies from "js-cookie";
import { AppActionType, AppState } from "../../store/reducers/AppReducer";
import { store } from "../../store/store";
export const SetMode = (mode: AppState["mode"]) => {
  store.dispatch({
    type: AppActionType,
    payload: { ...store.getState().app, mode: mode },
  });
  Cookies.set("mode", mode);
  setTimeout(() => {
    window.location.reload();
  }, 500);
};
