import Cookies from "js-cookie";
import { AppState } from "../../store/reducers/AppReducer";
export const GetMode = () => {
  return Cookies.get("mode") as AppState["mode"];
};
