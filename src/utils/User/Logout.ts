import Cookies from "js-cookie";
import { AppActionType } from "../../store/reducers/AppReducer";
import { store } from "../../store/store";

export const Logout = () => {
  store.dispatch({
    type: AppActionType,
    payload: { ...store.getState().app, User: undefined },
  });

  Cookies.remove("ui");
};
