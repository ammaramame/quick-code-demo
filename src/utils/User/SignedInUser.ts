import Cookies from "js-cookie";
import { User } from "../../models/User";
type SignedInUserType = () => User | undefined;
export const SignedInUser: SignedInUserType = () => {
  const EncodedUser = Cookies.get("ui");
  const stringUser = EncodedUser ? atob(EncodedUser) : undefined;
  const User = stringUser ? JSON.parse(stringUser) : undefined;
  return User;
};
