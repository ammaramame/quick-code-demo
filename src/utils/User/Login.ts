import { User } from "../../models/User";
import Cookies from "js-cookie";
import { store } from "../../store/store";
import { AppActionType } from "../../store/reducers/AppReducer";

export const Login = (User: User, remember: boolean) => {
  store.dispatch({
    type: AppActionType,
    payload: { ...store.getState().app, User: User },
  });

  if (remember) {
    const stringUser = JSON.stringify(User);
    const EncodedUser = btoa(stringUser);
    Cookies.set("ui", EncodedUser);
  }
};
