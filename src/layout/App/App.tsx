import { Box, CssBaseline, MuiThemeProvider } from "@material-ui/core";
import React from "react";
import Header from "../Header/Header";
import LoginPage from "../../pages/Login-Page/Login-Page";
import UsersPage from "../../pages/Users-Page/Users-Page";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import Page404 from "../../pages/Errors/404/Page-404";
import darkTheme from "../../themes/dark-theme";
import lightTheme from "../../themes/light-theme";
import { useSelector } from "react-redux";
import { stateType } from "../../store/store";

function App() {
  const { mode, User } = useSelector((state: stateType) => state.app);
  return (
    <MuiThemeProvider theme={mode === "dark" ? darkTheme : lightTheme}>
      <CssBaseline />
      <Box>
        <Header />
        <Box p={2}>
          <Router>
            <Switch>
              <Route path="/" component={User ? UsersPage : LoginPage} />
              <Route path="*" component={Page404} />
            </Switch>
          </Router>
        </Box>
      </Box>
    </MuiThemeProvider>
  );
}

export default App;
