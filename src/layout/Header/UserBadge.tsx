import { Avatar, Chip, Tooltip } from "@material-ui/core";
import * as React from "react";
import { FaPowerOff, FaUser } from "react-icons/fa";

interface UserBadgeProps {
  username: string;
  onLogoutClick?: () => any;
}

const UserBadge: React.SFC<UserBadgeProps> = (props) => {
  return (
    <Chip
      label={props.username}
      avatar={
        <Avatar>
          <FaUser />
        </Avatar>
      }
      deleteIcon={
        <Tooltip title="Logout" arrow>
          <FaPowerOff size={5} />
        </Tooltip>
      }
      onDelete={() => props.onLogoutClick?.()}
    />
  );
};

export default UserBadge;
