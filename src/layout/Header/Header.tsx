import { AppBar, Box, Toolbar, Typography } from "@material-ui/core";
import * as React from "react";
import { useSelector } from "react-redux";
import DarkModeSwitch from "../../components/Dark-Mode-Switch/Dark-Mode-Switch";
import { stateType } from "../../store/store";
import UserBadge from "./UserBadge";
import { Logout } from "../../utils/User/Logout";
import { SetMode } from "../../utils/Mode/Set-Mode";
export interface HeaderProps {}

const Header: React.SFC<HeaderProps> = () => {
  const { mode, User } = useSelector((state: stateType) => state.app);
  return (
    <AppBar position="relative" variant="outlined">
      <Toolbar>
        <Box width="100%" display="flex" justifyContent="space-between">
          <Typography variant="h6">Quick Demo TEST</Typography>
          {User && (
            <UserBadge
              username={User.username}
              onLogoutClick={() => Logout()}
            />
          )}
          <DarkModeSwitch
            checked={mode === "dark"}
            onChange={(check) => SetMode(check ? "dark" : "light")}
          />
        </Box>
      </Toolbar>
    </AppBar>
  );
};

export default Header;
