import axios from "axios";

const axiosInstance = axios.create({
  baseURL: "/",
});

// axiosInstance.defaults.headers.common["Authorization"] = `Bearer ${
//   Cookies.get("ui")
//     ? JSON.parse(atob(Cookies.get("ui"))).AuthInfo.access_token
//     : ""
// }`;

export { axiosInstance };
