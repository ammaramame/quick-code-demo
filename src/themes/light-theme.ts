import createTheme from "@mui/material/styles/createTheme";
const lightTheme = createTheme({
  palette: {
    mode: "light",
  },
});

export default lightTheme;
