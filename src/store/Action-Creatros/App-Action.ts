import { Dispatch } from "redux";
import { AppActionType, AppState } from "../reducers/AppReducer";

export const AppDataSetter =
  (AppData: Partial<AppState>) => (dispatcher: Dispatch) =>
    dispatcher({ type: AppActionType, payload: AppData });
