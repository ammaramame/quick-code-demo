import { User } from "../../models/User";
import { GetMode } from "../../utils/Mode/Get-Mode";
import { SignedInUser } from "../../utils/User/SignedInUser";

export interface AppState {
  mode: "light" | "dark";
  User: User | undefined;
}
export const AppActionType = "setAppState";
export interface AppAction {
  type: "setAppState";
  payload: AppState;
}
const AppReducer = (
  state: AppState = {
    mode: GetMode() ? GetMode() : "dark",
    User: SignedInUser() ? SignedInUser() : undefined,
  },
  action: AppAction
) => {
  return action.type === "setAppState"
    ? { ...state, ...action.payload }
    : state;
};

export { AppReducer };
