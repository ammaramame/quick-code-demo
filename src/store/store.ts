import { applyMiddleware, combineReducers } from "redux";
import { AppReducer } from "./reducers/AppReducer";
import { createStore } from "redux";
import thunk from "redux-thunk";

const CombinedReducers = combineReducers({
  app: AppReducer,
});
export const store = createStore(CombinedReducers, {}, applyMiddleware(thunk));

export type stateType = ReturnType<typeof CombinedReducers>;
