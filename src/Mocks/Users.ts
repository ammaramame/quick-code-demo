import { User } from "../models/User";

export const UsersArray: User[] = [
  {
    firstname: "Ammar",
    lastname: "Amami",
    email: "ammaramame@gmail.com",
    password: "123456789",
    username: "ammaramame",
  },
  {
    firstname: "Shanna",
    lastname: "Kelleher",
    email: "ShannaFKelleher@armyspy.com",
    password: "aj9oobooD7",
    username: "Usat1954",
  },
  {
    firstname: "Andrew",
    lastname: "Higginson",
    email: "AndrewTHigginson@jourrapide.com",
    password: "oeRuy4oh",
    username: "Regive49",
  },
  {
    firstname: "Ronnie",
    lastname: "Currier",
    email: "RonnieDCurrier@jourrapide.com",
    password: "Ohn4ahphie",
    username: "Opli1995",
  },
];
