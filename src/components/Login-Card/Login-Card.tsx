import {
  Checkbox,
  Box,
  Button,
  FormControlLabel,
  Grid,
  Paper,
  TextField,
} from "@material-ui/core";
import { Alert } from "@mui/material";
import * as React from "react";
import { useLoginCardProps } from "./Login-Card-Hook";

export interface LoginCardProps {
  PasswordValue: string;
  UsernameValue: string;
  OnButtonClick: () => any;
  PasswordvalueErrorHelperText?: string;
  UsernamevalueErrorHelperText?: string;
  RememberMeValue: boolean;
  DisableConfirm?: boolean;
  onPasswordChange: (e: React.ChangeEvent<HTMLInputElement>) => any;
  onUsernameChange: (e: React.ChangeEvent<HTMLInputElement>) => any;
  errorsArray?: string[];
  onRememberMeChange?: (checked: boolean) => any;
}

const LoginCard: React.SFC<LoginCardProps> = (Props) => {
  const { TextFieldsProps, Fields, ConfirmButtonProps } = useLoginCardProps();
  return (
    <Box component={Paper} p={2}>
      <Grid container spacing={2}>
        {Props.errorsArray?.map((item, index) => (
          <Grid key={index} item xs={12}>
            <Alert severity="warning">{item}</Alert>
          </Grid>
        ))}
        {Fields(Props).map((item, index) => (
          <Grid item xs={12} key={index}>
            <TextField {...TextFieldsProps(item, index)} />
          </Grid>
        ))}
        <Grid item xs={12}>
          <FormControlLabel
            control={
              <Checkbox
                checked={Props.RememberMeValue}
                onChange={(e, checked) => Props.onRememberMeChange?.(checked)}
              />
            }
            label="Remember Me"
          />
        </Grid>
        <Grid item xs={12}>
          <Button {...ConfirmButtonProps(Props)}>Login</Button>
        </Grid>
      </Grid>
    </Box>
  );
};

export default LoginCard;
