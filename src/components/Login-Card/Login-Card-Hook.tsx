import { ButtonProps, TextFieldProps } from "@material-ui/core";
import { LoginCardProps } from "./Login-Card";
interface IFields {
  label: string;
  placeholder: string;
  type: "text" | "password";
  value: string | boolean;
  helperText: string | undefined;
  onChange:
    | LoginCardProps["onUsernameChange"]
    | LoginCardProps["onPasswordChange"];
}
const Fields: (Props: LoginCardProps) => IFields[] = (Props) => [
  {
    label: "Username",
    placeholder: "Insert Username ...",
    type: "text",
    value: Props.UsernameValue,
    helperText: Props.UsernamevalueErrorHelperText,
    onChange: Props.onUsernameChange,
  },
  {
    label: "Password",
    placeholder: "Insert Password ...",
    type: "password",
    value: Props.PasswordValue,
    helperText: Props.PasswordvalueErrorHelperText,
    onChange: Props.onPasswordChange,
  },
];

const ConfirmButtonProps: (Props: LoginCardProps) => ButtonProps = (Props) => ({
  disabled: Props.DisableConfirm,
  variant: "contained",
  fullWidth: true,
  size: "large",
  color: "primary",
  onClick: Props.OnButtonClick,
});

const TextFieldsProps: (item: IFields, index: number) => TextFieldProps = (
  item,
  index
) => ({
  variant: "outlined",
  placeholder: item.placeholder,
  label: item.label,
  fullWidth: true,
  autoFocus: index === 0,
  type: item.type,
  error: Boolean(item.helperText),
  helperText: item.helperText,
  onChange: item.onChange,
});

export const useLoginCardProps = () => {
  return { TextFieldsProps, Fields, ConfirmButtonProps };
};
