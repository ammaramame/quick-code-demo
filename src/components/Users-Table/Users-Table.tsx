import { Box, Paper } from "@material-ui/core";
import MaterialTable from "material-table";
import * as React from "react";
import { UsersTableSchema } from "./Schema";

export interface UsersTableProps {}

const UsersTable: React.SFC<UsersTableProps> = () => {
  return (
    <Box component={Paper} p={2}>
      <MaterialTable {...UsersTableSchema()} />
    </Box>
  );
};

export default UsersTable;
