import { MaterialTableProps } from "material-table";
import { UsersArray } from "../../Mocks/Users";
import { User } from "../../models/User";
interface Fields extends User {}
interface Params {}
type UserSchemaType = (Params?: Params) => MaterialTableProps<Fields>;
const UsersTableSchema: UserSchemaType = () => {
  return {
    columns: [
      { field: "firstname", title: "Firstname" },
      { field: "lastname", title: "Lastname" },
      { field: "email", title: "Email" },
      { field: "username", title: "Username" },
    ],
    data: UsersArray,
    title: "Users Table",
    style: { boxShadow: "none" },
  };
};

export { UsersTableSchema };
